package com.asig2.app;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class Consumer implements CommandLineRunner {

    private final RabbitTemplate rabbitTemplate;

    public Consumer(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public void run(String... args) throws Exception {

        String file = "activities.txt";
        ObjectMapper mapper = new ObjectMapper();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        ArrayList<MonitoredData> list = new ArrayList<MonitoredData>();

        try (Stream<String> stream = Files.lines(Paths.get(file))) {

            list = (ArrayList<MonitoredData>) stream.map(s -> s.split("\\t\\t"))
                    .map(arr -> new MonitoredData(LocalDateTime.parse(arr[0], formatter),LocalDateTime.parse(arr[1], formatter),arr[2]))
                    .collect(Collectors.toCollection(ArrayList::new));

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Number of activities:" + list.size());


        for(MonitoredData monitoredData: list){
            mapper.registerModule(new JavaTimeModule());
            mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
            String jsonString = mapper.writeValueAsString(monitoredData);
            rabbitTemplate.convertAndSend(AppApplication.topicExchangeName, "foo.bar.baz", jsonString);
            System.out.println(jsonString);
        }
    }
}
